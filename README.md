# electron-wallpaper
A hack for windows to enable creating dynamic desktop wallpapers with electron.  
See the example [here](https://bitbucket.org/donnyofdoom/electron-wallpaper-quickstart) to see how it works

Heavily based off the work of [shundroid](https://github.com/shundroid/setup-wallpaper).  
Redone slightly to make it build for electron and work as intended.
